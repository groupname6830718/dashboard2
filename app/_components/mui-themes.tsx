import type {} from '@mui/x-data-grid/themeAugmentation';
import type {} from '@mui/x-data-grid-pro/themeAugmentation';
import type {} from '@mui/x-data-grid-premium/themeAugmentation';
import { createTheme } from '@mui/material/styles';

export const MUIAppDefaultTheme = createTheme({
  palette: {
    primary: {
      main: '#ffffff',
    },
    background: {
      paper: '#000000',
    },
  },
  typography: {
    fontFamily: 'var(--font-SFMono)',
  },
});
