'use client';
import { toNumberForm } from '@/app/_lib/toNumberForm';
import DataGrid from '@/app/_components/data-grid';

export default function PaymentsGrid({
  initialRows,
}: {
  initialRows: Array<Object>;
}) {
  return (
    <DataGrid
      columns={columns}
      rows={initialRows}
      initialState={initialState}

      className=''
    />
  );
}
const columns = [
  {
    field: 'place',
    headerName: 'name',
    //valueFormatter: (params) => getPlaceName(params, places),
    width: 168,
  },
  {
    field: 'amount',
    width: 90,
    type: 'number',
    valueFormatter: (x: { value: number }) => toNumberForm(x.value  / 100),
  },
  {
    field: 'name',
    headerName: 'payment',
    width: 90,
    headerAlign: 'right',
    align: 'right'

  },
  {
    field: 'status',
    width: 110,

    valueGetter: (x :  { value: string }) => {
      if (x.value === 'requires_payment_method' || x.value === "requires_action") {
        return 'unlocked';
      } else {
        return x.value;
      }
    },
  },


  {
    field: 'complete',
    width: 150,
  },
  {
    field: 'id',
    width: 150,
  },
];
const initialState = {
  columns: {
    columnVisibilityModel: {
      id: false,
      __check__: false,
      __reorder__: false,
      __detail_panel_toggle__: false,
      payment: false,
      processing: false,
      payment_failed: false,
      added: false,
      succeeded: false,
      requires_payment_method: false,
      complete: false,
    },
  },
  aggregation: {
    model: {
      amount: 'sum',
    },
  },
};
