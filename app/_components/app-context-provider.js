'use client';
import { createContext, useEffect, useState } from 'react';
import { MUIAppDefaultTheme } from './mui-themes';
import { ThemeProvider } from '@mui/material';

export const PlaceDataContext = createContext(null);
export const PaymentsDataContext = createContext(null);

export const firebaseConfig = {
  apiKey: 'AIzaSyCqK0UKHjqvttmZzIatEjLjfjX8P2gg1tE',
  authDomain: 'projectname-xxx.firebaseapp.com',
  projectId: 'projectname-xxx',
  storageBucket: 'projectname-xxx.appspot.com',
  messagingSenderId: '323589431735',
  appId: '1:323589431735:web:bdea4c7439419dbc578832',
};

export default function AppContextProvider({ children }) {
  const [placesData, setPlacesData] = useState(null);
  const [paymentsData, setPaymentsData] = useState(null);

  useEffect(() => {
    const initializeFirebase = async () => {
      const { initializeApp } = await import('firebase/app');
      const [firestoreModule] = await Promise.all([
        import('firebase/firestore'),
      ]);

      const firebaseApp = initializeApp(firebaseConfig);
      const firestore = firestoreModule.getFirestore(firebaseApp);

      const placesCollectionListener = firestoreModule.onSnapshot(
        firestoreModule.query(
          firestoreModule.collection(firestore, 'places'),
          firestoreModule.orderBy('value', 'desc')
        ),
        (querySnapshot) => {
          let placesData = {};
          querySnapshot.forEach((doc) => {
            placesData[doc.id] = doc.data();
          });
          console.log(placesData);
          setPlacesData(placesData);
        }
      );

      const paymentsCollectionListener = firestoreModule.onSnapshot(
        firestoreModule.query(
          firestoreModule.collection(firestore, 'payments'),
          firestoreModule.orderBy('created', 'desc')
        ),
        (querySnapshot) => {
          let paymentsData = {};
          querySnapshot.forEach((doc) => {
            paymentsData[doc.id] = doc.data();
          });
          console.log(paymentsData);
          setPaymentsData(paymentsData);
        }
      );

      return () => {
        placesCollectionListener();
        paymentsCollectionListener();
      };
    };

    initializeFirebase();
  }, []);

  return (
    <>
      <ThemeProvider theme={MUIAppDefaultTheme}>{children}</ThemeProvider>
    </>
  );
}
