'use client';
import { toNumberForm } from '@/app/_lib/toNumberForm';
import { useState } from 'react';
import DataGrid from '@/app/_components/data-grid';

export default function PlacesGrid({
  initialRows,
}: {
  initialRows: Array<Object>;
}) {
  const [width, setWidth] = useState(null);

  return (
    <DataGrid
      columns={columns}
      className=''
      rows={initialRows}
      initialState={initialState}
    />
  );
}

const initialState = {
  columns: {
    columnVisibilityModel: {
      id: false,
      __check__: false,
      __reorder__: false,
      stripe_customer_id: false,
      unit: false,
      url: false,
      coordinates: false,
      under_development: false,
      logins: false,
      autopay_payment_method: false,
      payment_timing: false,
      payment_methods: false,
      applications_open: true,
      primary_email: false,
      autopay: false,
      place: false,
      primaryEmail: false,
      selectedPaymentMethod: false,
      inside: false,
      outside: false,
    },
  },
  aggregation: {
    model: {
      price: 'sum',
      value: 'sum',
    },
  },
  filter: {
    filterModel: {
      items: [
        // { field: 'under_development', operator: 'is', value: 'true', id: 'fsd' },
      ],
    },
  },
};

const columns = [
  {
    field: 'name',
    width: 168,
  },
  {
    editable: true,
    field: 'price',
    width: 90,
    type: 'number',
    valueFormatter: (x: { value: number }) => toNumberForm(x.value),
  },
  {
    field: 'value',
    editable: true,
    width: 90,
    type: 'number',
    valueFormatter: (x: { value: number }) => toNumberForm(x.value),
  },
  {
    field: 'applications_open',
    headerName: 'vacant',
    width: 70,
    type: 'boolean',
    editable: true,
  },
  {
    field: 'inside',
    width: 90,
    type: 'number',
    valueFormatter: (x: { value: number }) => toNumberForm(x.value),
    editable: true,
  },
  {
    field: 'outside',
    valueFormatter: (x: { value: number }) => toNumberForm(x.value),

    width: 90,
    type: 'number',
    editable: true,
  },
  {
    field: 'payment_timing',
    headerName: 'init payment',
    width: 120,

    type: 'number',
  },
  {
    field: 'autopay',
    width: 77,
    type: 'boolean',
  },
  {
    field: 'url',
    width: 140,
  },
  {
    field: 'autopay_payment_method',
    headerName: 'autopay payment method',
    width: 0,
    type: 'object',
  },
  {
    field: 'payment_methods',
    headerName: 'payment methods',
    width: 0,
  },
  {
    field: 'logins',
    width: 0,
  },
  {
    field: 'primary_email',
    headerName: 'primary email',
    editable: true,
    width: 125,
  },
  {
    field: 'unit',
    width: 81,
  },
  {
    field: 'stripe_customer_id',
    headerName: 'stripe cust id',
    width: 150,
  },
  {
    field: 'id',
    headerName: 'address',
    width: 300,
  },
  {
    field: 'under_development',
    headerName: 'realized',
    width: 85,
    editable: true,
    type: 'boolean',
  },
];
