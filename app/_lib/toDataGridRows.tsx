const toDataGridRows = (obj: Object) =>
  Object.entries(obj).map(([id, data]) => ({ id, ...data }));

export default toDataGridRows;
