import PlacesGrid from '@/app/_components/places-grid';
import toDataGridRows from '@/app/_lib/toDataGridRows';
import AppContextProvider from '@/app/_components/app-context-provider';
import PaymentsGrid from '@/app/_components/payments-grid';

export default async function Home() {
  const initialPlacesGridRows = toDataGridRows(
    await fetch('https://www.units-server.com/api/places')
      .then((res) => res.json())
      .then((data: Object) => {
        return Object.values(data).map(
          (place: {
            name: string;
            price: number;
            value: number;
            applications_open: boolean;
          }) => ({
            name: place.name,
            price: place.price,
            value: place.value,
            applications_open: place.applications_open,
          })
        );
      })
  );

  const initalPaymentsGridRows = toDataGridRows(
    await fetch('https://www.units-server.com/api/payments')
      .then((x) => x.json())
      .then((data: Object) => {
        return Object.values(data).map(
          (payment: {
            place: string;
            amount: number;
            name: string;
            status: string;
          }) => ({
            place: payment.place,
            amount: payment.amount,
            name: payment.name,
            status: payment.status,
          })
        );
      })
  );

  return (
    <main className='min-h-screen bg-black flex overflow-auto'>
    <AppContextProvider>
      <div className="w-1/4  ">
        <PlacesGrid initialRows={initialPlacesGridRows} />
      </div>
      <div className="mx-auto w-px h-auto ">  {/* Spacer */}
      </div>
      <div className="w-1/4  ">
        <PaymentsGrid initialRows={initalPaymentsGridRows} />
      </div>
    </AppContextProvider>
    </main>

    );


}
